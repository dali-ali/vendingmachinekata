# VendingMachineKata

## Introduction

Ce projet est une simulation d'un distributeur automatique. Il a été créé pour pratiquer la méthodologie de développement piloté par les tests et pour mettre en œuvre différentes fonctionnalités d'un distributeur automatique.

## Fonctionnalités implémentées

### Étape 1: Mise en place du projet

- Création d'un projet Typescript pour le distributeur automatique.
- Initialisation d'un dépôt Git pour suivre les modifications du code.

### Étape 2: Accepter les pièces

- Le distributeur automatique accepte les pièces valides (nickels, dimes, quarters).
- Les pièces invalides (pennies) sont rejetées et placées dans le retour de monnaie.
- Des tests unitaires ont été écrits pour vérifier le bon fonctionnement de cette fonctionnalité.

### Étape 3: Sélectionner un produit

- Les clients peuvent sélectionner des produits (cola, chips, candy) en appuyant sur le bouton correspondant.
- Si le montant inséré est suffisant pour le produit sélectionné, il sera délivré, et le message "THANK YOU" s'affichera sur l'écran.
- Si le montant inséré est supérieur au prix du produit, la monnaie rendue sera affichée à l'écran.
- Si le montant inséré est insuffisant, l'affichage indiquera le prix du produit et le montant actuel.

### Étape 4: Rendre la monnaie

- Lorsqu'un produit coûte moins que le montant d'argent dans la machine, le montant restant est rendu sous forme de pièces.

### Étape 5: Récupérer les pièces

- Les clients peuvent récupérer les pièces insérées en appuyant sur le bouton "rendre les pièces".

### Étape 6: Produit épuisé

- Si le produit sélectionné est en rupture de stock, le distributeur affiche "SOLD OUT".

### Étape 7: Exact Change Only
- Si le distributeur n'a pas de pièces de monnaie (quarters, dimes, nickels), il affiche "EXACT CHANGE ONLY" à la place de "INSERT COIN".

## Guide d'utilisation

Le distributeur automatique est facile à utiliser :

- Insérez les pièces valides (nickels, dimes, quarters) dans la fente prévue à cet effet.
- Sélectionnez le produit souhaité (cola, chips, candy) en appuyant sur le bouton correspondant.
- Si vous avez inséré suffisamment d'argent pour le produit sélectionné, il sera délivré, et le message "THANK YOU" s'affichera sur   l'écran.
- Si le montant inséré est supérieur au prix du produit, la monnaie rendue sera affichée à l'écran.
- Pour annuler l'achat ou récupérer les pièces insérées, appuyez sur le bouton "rendre les pièces".
- Si le produit sélectionné est en rupture de stock, le message "SOLD OUT" s'affichera à l'écran.

## Choix de conception

Nous avons choisi d'utiliser TypeScript pour ce projet afin de bénéficier de la typisation statique et d'une meilleure lisibilité du code. Nous avons également utilisé Jest comme framework de test pour écrire les tests unitaires.

Pour la gestion des pièces, nous avons utilisé un tableau de pièces valides et un objet contenant les valeurs des pièces acceptées.

## Tests

Nous avons écrit des tests unitaires pour chaque fonctionnalité implémentée jusqu'à présent. Les tests vérifient que le distributeur automatique accepte correctement les pièces valides et rejette les pièces invalides
Test à faire : sélectionne et délivre les produits en fonction de l'argent inséré, et gère les cas de produits en rupture de stock.

## Temps passé

- Étape 2: Accepter les pièces : 2 heures (incluant les tests)
- Étape 3 et 6: Sélectionner un produit et Produit épuisé : 2 heures (incluant les tests)
- Étape 4 et 5: Rendre la monnaie : 1 heures (incluant les tests)
- Étape 7 : Exact Change Only : 1 heures (incluant les tests)

## Installation

Pour exécuter ce projet localement, assurez-vous d'avoir Node.js installé. Clonez ensuite le dépôt et exécutez les commandes suivantes :

    npm install
    npm test

