import VendingMachine from './VendingMachine';


describe('VendingMachine - Insert Coin', () => {
  let mockVendingMachine: VendingMachine;

  beforeEach(() => {
    mockVendingMachine = new VendingMachine();
  });

  it('should accept a nickel', () => {
    mockVendingMachine.insertCoin('nickel');
    expect(mockVendingMachine['lastAmountAded']).toBe(0.05);
  });

  it('should accept a dime', () => {
    mockVendingMachine.insertCoin('dime');
    expect(mockVendingMachine['lastAmountAded']).toBe(0.10);
  });

  it('should accept a quarter', () => {
    mockVendingMachine.insertCoin('quarter');
    expect(mockVendingMachine['lastAmountAded']).toBe(0.25);
  });

  it('should reject an invalid coin - pennies', () => {
    mockVendingMachine.insertCoin('pennies');
    expect(mockVendingMachine['lastAmountAded']).toBe(0); // The total amount should remain 0.
  });

  it('should reject an invalid coin - other invalid coin', () => {
    mockVendingMachine.insertCoin('invalid');
    expect(mockVendingMachine['lastAmountAded']).toBe(0); // The total amount should remain 0.
  });
});

describe('VendingMachine - Select Product', () => {
  let mockVendingMachine: VendingMachine;
  beforeEach(() => {
    mockVendingMachine = new VendingMachine();

  });

  it('should dispense the product and display "THANK YOU" if enough money is inserted', () => {
    const logSpy = jest.spyOn(console, 'log');
    mockVendingMachine.insertCoin('quarter'); // Insert 25 cents
    mockVendingMachine.insertCoin('quarter'); // Insert another 25 cents (total: 50 cents)
    mockVendingMachine.insertCoin('quarter'); // Insert another 25 cents (total: 75 cents)
    mockVendingMachine.insertCoin('quarter'); // Insert another 25 cents (total: $1.00)

    // Select a product with price $1.00
    mockVendingMachine.selectProduct('cola');

    // Product should be dispensed
    expect(console.log).toHaveBeenCalledWith('Product delivered: cola');
    expect(console.log).toHaveBeenCalledWith('THANK YOU');

    // The total amount should be reset to $0.00, and display should show "INSERT COIN"
    expect(mockVendingMachine['totalAmount']).toBe(0);

    logSpy.mockRestore();
  });

  it('should display "PRICE", the price of the product, the "current Product" and the "Amount remaining" if not enough money is inserted', () => {
    const logSpy = jest.spyOn(console, 'log');
    mockVendingMachine.insertCoin('quarter'); // Insert 25 cents
    mockVendingMachine.insertCoin('quarter'); // Insert another 25 cents (total: 50 cents)

    // Select a product with price $1.00, but only 50 cents inserted
    mockVendingMachine.selectProduct('cola');

    // Display should show "PRICE: $1.00"
    expect(console.log).toHaveBeenCalledWith("PRICE: $1.00");
    expect(console.log).toHaveBeenCalledWith('Current amount: $0.50');
    expect(console.log).toHaveBeenCalledWith('Amount remaining: $0.50');

    // The total amount should remain unchanged, and display should show the current amount
    expect(mockVendingMachine['totalAmount']).toBe(0.50);

    logSpy.mockRestore();
  });

  it('should display "SOLD OUT" if the selected product is not available', () => {
    const logSpy = jest.spyOn(console, 'log');
    // Select a product that is not available
    mockVendingMachine.selectProduct('water');

    // Display should show "SOLD OUT"
    expect(console.log).toHaveBeenCalledWith('SOLD OUT');

    // The total amount should remain unchanged, and display should show "INSERT COIN"
    expect(mockVendingMachine['totalAmount']).toBe(0);

    logSpy.mockRestore();
  });

  it('should display "INSERT COIN" if no product is selected', () => {
    const logSpy = jest.spyOn(console, 'log');
    // No product is selected
    mockVendingMachine.selectProduct('');

    // Display should show "INSERT COIN"
    expect(console.log).toHaveBeenCalledWith('SELECT PRODUCT');

    logSpy.mockRestore();
  });

});


describe('VendingMachine - Return Coins', () => {
  let mockVendingMachine: VendingMachine;

  beforeEach(() => {
    mockVendingMachine = new VendingMachine();
  });

  it('should return the correct number of coins for $0.35', () => {
    const amount = 0.35;
    const logSpy = jest.spyOn(console, 'log');

    mockVendingMachine.returnCoins(amount);

    expect(logSpy).toHaveBeenCalledWith('Returned coins: $0.35');
    expect(logSpy).toHaveBeenCalledWith('1 quarter');
    expect(logSpy).toHaveBeenCalledWith('1 dime');
    expect(logSpy).toHaveBeenCalledTimes(4); // Verify that console.log was called 4 times

    logSpy.mockRestore();
  });

  it('should return the correct number of coins for $0.20', () => {
    const amount = 0.20;
    const logSpy = jest.spyOn(console, 'log');

    mockVendingMachine.returnCoins(amount);

    expect(logSpy).toHaveBeenCalledWith('Returned coins: $0.20');
    expect(logSpy).toHaveBeenCalledWith('2 dimes');
    expect(logSpy).toHaveBeenCalledTimes(3);

    logSpy.mockRestore();
  });

  it('should return the correct number of coins for $0.10', () => {
    const amount = 0.10;
    const logSpy = jest.spyOn(console, 'log');

    mockVendingMachine.returnCoins(amount);

    expect(logSpy).toHaveBeenCalledWith('Returned coins: $0.10');
    expect(logSpy).toHaveBeenCalledWith('1 dime');
    expect(logSpy).toHaveBeenCalledTimes(3);

    logSpy.mockRestore();
  });

  it('should return the correct number of coins for $0.05', () => {
    const amount = 0.05;
    const logSpy = jest.spyOn(console, 'log');

    mockVendingMachine.returnCoins(amount);

    expect(logSpy).toHaveBeenCalledWith('Returned coins: $0.05');
    expect(logSpy).toHaveBeenCalledWith('1 nickel');
    expect(logSpy).toHaveBeenCalledTimes(3);

    logSpy.mockRestore();
  });
});


describe("VendingMachine - Exact Change Only", () => {
  let mockVendingMachine: VendingMachine;

  beforeEach(() => {
    mockVendingMachine = new VendingMachine();
  });



  it("should not display 'EXACT CHANGE ONLY' and return false when the machine has coins", () => {
    const logSpy = jest.spyOn(console, 'log');
    const vendingMachine = new VendingMachine();

  // Ensure that the coin inventory is empty
  vendingMachine.setCoinInventory({
    quarter: [1],
    dime: [2],
    nickel: [4],
  });

  // Act
  const result = vendingMachine.hasAllCoins();

  // Assert
  expect(result).toBe(true);
  expect(console.log).not.toHaveBeenCalledWith("EXACT CHANGE ONLY");
    logSpy.mockRestore();
  });

  it("should return false when the machine has no coins", () => {
    const logSpy = jest.spyOn(console, 'log');
    const vendingMachine = new VendingMachine();

  vendingMachine.setCoinInventory({
    quarter: [],
    dime: [],
    nickel: [],
  });

  const result = vendingMachine.hasAllCoins();

  expect(result).toBe(false);
  // expect(console.log).toHaveBeenCalledWith("EXACT CHANGE ONLY");
    logSpy.mockRestore();
  });

});

