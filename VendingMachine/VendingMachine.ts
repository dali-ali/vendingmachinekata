class VendingMachine {
  private totalAmount: number;
  private lastAmountAded: number;
  private coinInventory: { [key: string]: number[] };

  constructor() {
    this.totalAmount = 0;
    this.lastAmountAded = 0;
    this.coinInventory = {
      quarter: [],
      dime: [],
      nickel: [],
    };
  }

  
  setCoinInventory(coinInventory: { [key: string]: number[] }) {
    this.coinInventory = coinInventory;
  }

  /**
   * Insère une pièce dans le distributeur automatique.
   * Mettre à jour le montant total en fonction du type de pièce insérée.
   * Si la pièce est invalide, elle est placée dans le retour de monnaie.
   * @param coinType Le type de pièce insérée (par exemple, "nickel", "dime" ou "quarter").
   */
  insertCoin(coinType: string) {
    const validCoins = ["nickel", "dime", "quarter"];
    const coinValues = {
      nickel: 0.05,
      dime: 0.10,
      quarter: 0.25,
    };

    if (validCoins.includes(coinType)) {
      this.lastAmountAded = coinValues[coinType]
      this.totalAmount += coinValues[coinType];
      console.log("Current amount : ", this.totalAmount);
    } else {
      // Si la pièce est invalide, la placer dans le retour de monnaie.
      if (coinType === "pennies") {
        this.lastAmountAded = 0;
        console.log("Coin rejected: pennies");
        console.log("Current amount : ", this.totalAmount);
      } else {
        this.lastAmountAded = 0;
        console.log(`Coin rejected: ${coinType}`);
        console.log("Current amount : ", this.totalAmount);
      }
    }
  }

  hasAllCoins(): boolean {
    const coinsAvailable = {
      quarter: false,
      dime: false,
      nickel: false,
    };

    // Vérifier si le distributeur a au moins une pièce de chaque type
    for (const coinType of Object.keys(coinsAvailable)) {
      if (this.coinInventory[coinType].length > 0) {
        coinsAvailable[coinType] = true;
      }
    }
    const coinInventory = this.coinInventory;
    if (!coinInventory) {
      console.log("EXACT CHANGE ONLY");
    }
    // Vérifier si toutes les pièces sont disponibles
    return Object.values(coinsAvailable).every((available) => available);
  }

  /**
 * Sélectionne un produit dans le distributeur automatique.
 * Si le produit est disponible et le montant inséré est suffisant,
 * le produit est délivré et "THANK YOU" est affiché.
 * Sinon, affiche "PRICE" suivi du prix du produit.
 * Après la sélection, rendre le monnais si existe, remet le montant inséré à zéro et affiche "INSERT COIN".
 * @param productName Le nom du produit sélectionné par le client (par exemple, "cola", "chips" ou "candy").
 */
  selectProduct(productName: string) {
    if (!this.hasAllCoins()) {
      console.log("EXACT CHANGE ONLY");
    }
    const products = {
      cola: { name: "cola", price: 1.00 },
      chips: { name: "chips", price: 0.50 },
      candy: { name: "candy", price: 0.65 },
    };

    // Vérifie si le produit est disponible dans le distributeur
    if (products.hasOwnProperty(productName)) {
      const selectedProduct = products[productName];
      const productPrice: number = selectedProduct.price;


      if (this.totalAmount == 0) {
        console.log(" INSERT COIN")
      }

      // Vérifie si le montant inséré est suffisant pour acheter le produit
      if (this.totalAmount >= productPrice) {
        // Délivre le produit
        console.log(`Product delivered: ${selectedProduct.name}`);
        console.log("THANK YOU");
        if (this.hasAllCoins()) {
          // Rendre la monnaie si le montant inséré est supérieur au prix du produit
          const change = this.totalAmount - productPrice;
          if (change > 0) {
            this.returnCoins(change);
          }
        }

        // Remet le montant inséré à zéro et affiche "INSERT COIN"
        this.totalAmount = 0;
        console.log("INSERT COIN");

      } else {// Le client a sélectionné un produit, mais le montant inséré est insuffisant
        // Montant insuffisant, affiche le prix du produit
        console.log(`PRICE: $${productPrice.toFixed(2)}`);
        // Affiche le montant actuel pour rappeler le client
        console.log(`Current amount: $${this.totalAmount.toFixed(2)}`);
        // Affiche le montant qui manque pour acheter le produit
        console.log(`Amount remaining: $${(productPrice - this.totalAmount).toFixed(2)}`);
      }

    } else {
      if (productName == "") {
        console.log("SELECT PRODUCT")
      }
      // Le produit n'est pas disponible, affiche "SOLD OUT"
      else {
        console.log("SOLD OUT");
        if (this.totalAmount) {
          console.log("Current amount : ", this.totalAmount);
        } else {
          console.log(" INSERT COIN")
        }
      }
    }

  }

  returnCoins(amount: number) {
    console.log(`Returned coins: $${amount.toFixed(2)}`); /// toFixed(2) : deux chiffre apres le virgule

    // Convert dollars to cents
    let cents = Math.round(amount * 100);

    // Calculate the number of quarters to return
    const numQuarters = Math.floor(cents / 25);
    cents %= 25;

    // Calculate the number of dimes to return
    const numDimes = Math.floor(cents / 10);
    cents %= 10;

    // Calculate the number of nickels to return
    const numNickels = Math.floor(cents / 5);

    // Display the number of each coin type to return
    if (numQuarters > 0) {
      console.log(`${numQuarters} quarter${numQuarters > 1 ? 's' : ''}`);
    }
    if (numDimes > 0) {
      console.log(`${numDimes} dime${numDimes > 1 ? 's' : ''}`);
    }
    if (numNickels > 0) {
      console.log(`${numNickels} nickel${numNickels > 1 ? 's' : ''}`);
    }

    this.totalAmount = 0;
    console.log("INSERT COIN");
  }
}

export default VendingMachine;
